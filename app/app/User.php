<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'location',
    ];

    /**
     * Сохраняет пользователя
     *
     * @param string $name
     * @param string $email
     * @param string $location
     * @return static
     */
    public static function store(string $name, string $email, string $location): self
    {
        $user = new static;
        $user->name = $name;
        $user->email = $email;
        $user->location = $location;

        $user->save();

        return $user;
    }

}
