# Решаемая задача

Приложение позволяет сохранить пользователя в базу данных и 
отправить результат в запрошенную точку обмена и очередь

# Начало работы

1. Склонировать проект
`git clone git@gitlab.com:thezibun/ndlab_queue.git`

2. Настроить .env
`cd app && cp .env.example .env`

   По умолчанию значения для БД и очереди уже заданы.
   Однако можно их задать и в .env файле.

   Настройки БД находятся в файле
   `app/app/config/database.php` в секции `pgsql`

   Настройки RabbitMQ находятся в файле
   `app/app/config/queue.php` в секции `rabbitmq`

3. Из корня проекта собрать и запустить докер командой 
`docker-compose up -d`

4. Установить зависимости командой
`docker-compose exec app composer install`

5. Сгенерировать ключ приложения командой
`docker-compose exec app  php artisan key:generate`

6. Применить миграции

   `docker-compose exec app  php artisan migrate`

# Запуск слушателя

Для того, чтобы запустить прослушивание очереди, нужно выполнить команду
`docker-compose exec app  php artisan rabbitevents:listen user.add`

# Отправка данных

Данные отправляются методом POST, тело запроса - в формате JSON.
Адрес: 

`http://neurodata.localhost/api/user/add`

Пример корректных данных: 

```javascript
{
	"action" : "add_user",
	"name" : "Test",
	"location" : "City",
	"email": "testmail@mail.com",
	"reply_to" : {
		"queue" : "squeue",	
		"exchange" : "some_exchange"		
	}
}
```

Для теста можно использовать curl или, например, Postman

# Мониторинг

Для доступа к базе можно перейти по адресу:

`http://neurodata.localhost:8080/`

Данные для входа (если не были изменены):

* Движок - PostgreSQL
* Сервер - db
* Имя пользователя - postgres
* Пароль - password
* База данных - postgres

Для доступа к RabbitMQ можно перейти по адресу:

`http://neurodata.localhost:15672/`

* Имя пользователя и пароль - guest

# Инструменты

Использованы следующие инструменты:
 * Laravel (версия 6.7, php 7.2)
 * PostgreSQL 12.1
 * RabbitMQ 3.8
 * RabbitEvents для AMPQ
 
# Код решения
 
 * Обработчик очереди:
 
 `app/app/QueueListeners/UserAdd.php`
 
 * Контроллер
 
 `app/app/Http/Controllers/Api/UserController.php`
 
 # Переменные 
 
 В файле `app/.env` можно настроить следующие группы переменных.
 
 1. База данных:
 
 * 'DB_HOST' - хост базы данных. По умолчанию - 'db',
 * 'DB_PORT' - порт базы данных. По умолчанию - '5432',
 * 'DB_DATABASE' - название базы данных. По умолчанию - 'postgres'
 * 'DB_USERNAME' - имя пользователя. По умолчанию - 'postgres'
 * 'DB_PASSWORD' - пароль. По умолчанию - 'password'
 
 2. RabbitMQ: 
 
 * 'RABBITMQ_EXCHANGE' - точка обмена. По умолчанию - 'events'
 * 'RABBITMQ_HOST' - хост. По умолчанию - 'rmq'
 * 'RABBITMQ_PORT' - порт. По умолчанию - 5672
 * 'RABBITMQ_USER' - пользователь. По умолчанию - 'guest'
 * 'RABBITMQ_PASSWORD' - пароль. По умолчанию - 'guest'
 * 'RABBITMQ_VHOST' - vhost неймспейс. По умолчанию - '/'
 * 'RABBITMQ_QUEUE' - очередь. По умолчанию - 'user.add'